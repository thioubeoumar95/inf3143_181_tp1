# TP1: OCL - FAR ★ STAR

Voir le sujet [ici](http://info.uqam.ca/~terrasa/cours/INF3143/tp1/).

## Tester ses contraintes

Le répertoire `/tests` contient quelques tests publics:
* `test_XX.in`: fichier d'instance
* `test_XX.res`: résultat attendu

Pour lancer un test en ligne de commandes:

	$> use -nogui -nr -qv model.use tests/test_XX.in

Il est possible de comparer rapidement votre résultat grâce à la commande `diff`:

	$> use -nogui -nr -qv model.use tests/test_XX.in > mon_res
	$> diff tests/test_XX.res mon_res

Cependant, et comme je l'expliquais en classe, vos résultats peuvent différer des
miens en fonction de l'ordre de vos contraintes et de comment vous les avez implémentées.
Ainsi, une différence entre votre résultat et le mien ne signifie pas forcément que
votre solution est fausse.

Pour la correction, les différences ne sont pas comptabilisées, seulement le fait que votre
contrainte est écrite correctement et qu'elle produit le résultat attendu.

## Tests automatisés

Le script `test_team.sh` permet de tester automatiquement votre fichier de modèle avec les tests publics fournis.

Commencez par implémenter le fichier `model.use` dans le répertoire racine.

Pour lancer le script:

	$> ./check_team.sh

Va vérifier chacun des tests avec les résultats attendus.
