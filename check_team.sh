#!/bin/bash
#
# Copyright 2016 Alexandre Terrasa <alexandre@moz-code.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

model="model.use"

mkdir -p res/

# Check test 0, program compile without error
use -nogui -nr -t -c $model > res/comp.res 2>&1
if [ -s res/comp.res ]; then
	echo -ne "[\e[31mERR\e[0m] "
	echo -ne "\e[1mCOMPILING\e[0m "
	echo -e "(cat res/comp.res)"
	exit 1
fi

# Run tests
for testfile in `ls tests/*.in`; do
	echo -ne " * "
	testbase=`basename $testfile`
	test="${testbase%.*}"

	# Run test
	use -nogui -nr -t -qv $model $testfile > res/$test.res 2>&1

	# Check empty output
	if [ ! -f res/$test.res ]; then
		echo -ne "[\e[31mERR\e[0m] "
		echo -ne "\e[1m$test\e[0m "
		echo -e "(cat res/$test.res)"
		continue
	fi

	# Check sav
	if [ ! -f tests/$test.res ]; then
		echo -ne "[SAV] "
		echo -ne "\e[1m$test\e[0m "
		echo -e "(not .res file for $test)"
		continue
	fi

	# Check diff
	diff tests/$test.res res/$test.res > res/$test.diff
	if [ -s res/$test.diff ]; then
		echo -ne "[\e[31mKO\e[0m] "
		echo -ne "\e[1m$test\e[0m "
		echo -e "(meld tests/$test.res res/$test.res)"
	else
		echo -ne "[\e[32mOK\e[0m] "
		echo -e "\e[1m$test\e[0m"
	fi
done
